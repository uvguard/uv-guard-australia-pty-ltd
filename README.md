Water and air purification business specializing in UV disinfection and other sustainable treatment technologies. Supplier of complete water and air treatment solutions, spare parts for all UV disinfection system brands and models, technical adviser in the field of water and air treatment.

Address: 3/44 Carrington Road, Castle Hill, NSW 2154, Australia

Phone: +61 2 9631 4900